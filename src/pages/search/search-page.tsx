import { useEffect, useState } from 'react';
import { Dimmer, Loader, Container } from 'semantic-ui-react';
import { Restaurant } from "../../types";
import { RestauranteService } from "../../services";
import { RestaurantList } from '../../component';
import { RestaurantHandler } from '../../handlers';

const getRestaurants = async () => (
    await RestauranteService.getByPostCode()
)

const SearchPage = ({ }) => {
    const [restaurants, setRestaurants] = useState([] as Restaurant[])
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        if (restaurants.length == 0) {
            getRestaurants().then(respRestaurants => {
                const convertedRestaurants = RestaurantHandler.convertRestaurantResponses(
                    respRestaurants.Restaurants
                )
                setRestaurants(convertedRestaurants)
            })
        }

        if (restaurants.length > 0) {
            setLoading(false)
        }
    }, [restaurants, isLoading])


    return restaurants.length == 0 && isLoading ?
        (
            <Dimmer active>
                <Loader>Loading...</Loader>
            </Dimmer>
        )
        : (
            <Container text>
                <p>Title</p>
                <RestaurantList restaurants={restaurants} />
            </Container>
        )
}

export default SearchPage;
