import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { SearchPage } from "../pages";

const routes = (
    <Router>
        <Routes>
            <Route path="/" element={<SearchPage />} />
        </Routes>
    </Router>
)

export default routes;
