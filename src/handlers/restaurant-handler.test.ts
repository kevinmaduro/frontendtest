import RestaurantHandler from './restaurant-handler';
import { CuisineType, Restaurant } from "../types";

const expectCuisineType = expect.objectContaining({
    id: expect.any(Number),
    seoName: expect.any(String),
    name: expect.any(String)
} as CuisineType)

const expectRestaurantObject = expect.objectContaining({
    id: expect.any(Number),
    uniqueName: expect.any(String),
    rating: expect.any(Number),
    logo: expect.any(String),
    name: expect.any(String),
    cuisineTypes: expect.arrayContaining([
        expectCuisineType
    ])
} as Restaurant)

const restaurantExample = {
    "Id": 143490,
    "Name": "LEON - Bankside",
    "UniqueName": "leonbankside-southwark",
    "Address": {
        "City": "London",
        "FirstLine": "7 Canvey Street",
        "Postcode": "SE1 9AN",
        "Latitude": 51.506482,
        "Longitude": -0.099539
    },
    "City": "London",
    "Postcode": "SE1 9AN",
    "Latitude": 0,
    "Longitude": 0,
    "Rating": {
        "Count": 90,
        "Average": 4.33,
        "StarRating": 4.33
    },
    "RatingStars": 4.33,
    "NumberOfRatings": 90,
    "RatingAverage": 4.33,
    "Description": "",
    "Url": "https://www.just-eat.co.uk/restaurants-leonbankside-southwark",
    "LogoUrl": "http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/143490.gif",
    "IsTestRestaurant": false,
    "IsHalal": false,
    "IsNew": false,
    "ReasonWhyTemporarilyOffline": "",
    "DriveDistance": 0.5,
    "DriveInfoCalculated": true,
    "IsCloseBy": false,
    "OfferPercent": 0,
    "NewnessDate": "2021-03-05T17:42:22.861594",
    "OpeningTime": "2022-03-27T10:23:00Z",
    "OpeningTimeUtc": null,
    "OpeningTimeIso": "2022-03-27T10:23:00",
    "OpeningTimeLocal": "2022-03-27T11:23:00",
    "DeliveryOpeningTimeLocal": "2022-03-27T09:23:00",
    "DeliveryOpeningTime": "2022-03-27T08:23:00Z",
    "DeliveryOpeningTimeUtc": null,
    "DeliveryStartTime": "2022-03-27T08:23:00Z",
    "DeliveryTime": null,
    "DeliveryTimeMinutes": null,
    "DeliveryWorkingTimeMinutes": 23,
    "DeliveryEtaMinutes": {
        "Approximate": null,
        "RangeLower": 30,
        "RangeUpper": 45
    },
    "IsCollection": false,
    "IsDelivery": true,
    "IsFreeDelivery": false,
    "IsOpenNowForCollection": false,
    "IsOpenNowForDelivery": true,
    "IsOpenNowForPreorder": false,
    "IsOpenNow": true,
    "IsTemporarilyOffline": false,
    "DeliveryMenuId": null,
    "CollectionMenuId": null,
    "DeliveryZipcode": null,
    "DeliveryCost": 0.99,
    "MinimumDeliveryValue": 0,
    "SecondDateRanking": 0,
    "DefaultDisplayRank": 0,
    "SponsoredPosition": 0,
    "SecondDateRank": 0,
    "Score": 1200,
    "IsTemporaryBoost": true,
    "IsSponsored": false,
    "IsPremier": false,
    "HygieneRating": null,
    "ShowSmiley": false,
    "SmileyDate": null,
    "SmileyElite": false,
    "SmileyResult": null,
    "SmileyUrl": null,
    "SendsOnItsWayNotifications": false,
    "BrandName": "Leon",
    "IsBrand": true,
    "LastUpdated": "2022-03-27T05:28:04.642033",
    "Deals": [],
    "Offers": [],
    "Logo": [
        {
            "StandardResolutionURL": "http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/143490.gif"
        }
    ],
    "Tags": [
        "fsa",
        "low-delivery-fee",
        "open_now"
    ],
    "DeliveryChargeBands": [],
    "CuisineTypes": [
        {
            "Id": 79,
            "IsTopCuisine": true,
            "Name": "Chicken",
            "SeoName": "chicken"
        },
        {
            "Id": 89,
            "IsTopCuisine": false,
            "Name": "Healthy",
            "SeoName": "healthy"
        },
        {
            "Id": 1004,
            "IsTopCuisine": false,
            "Name": "Low Delivery Fee",
            "SeoName": "low-delivery-fee"
        }
    ],
    "Cuisines": [
        {
            "Name": "Chicken",
            "SeoName": "chicken"
        },
        {
            "Name": "Healthy",
            "SeoName": "healthy"
        },
        {
            "Name": "Low Delivery Fee",
            "SeoName": "low-delivery-fee"
        }
    ],
    "ScoreMetaData": [
        {
            "Key": "Distance",
            "Value": "0.5"
        },
        {
            "Key": "ExpIsRds",
            "Value": "true"
        },
        {
            "Key": "SetName",
            "Value": "TempBoost"
        }
    ],
    "Badges": [],
    "OpeningTimes": [],
    "ServiceableAreas": []
}

const restaurantExamples = [
    restaurantExample,
    restaurantExample,
    restaurantExample
]

describe('Restaurant Handler', () => {
    describe('func convertRestaurantResponse', () => {
        it('should convert the response into expected object', () => {
            const restaurant = RestaurantHandler.convertRestaurantResponse(restaurantExample);
            expect(restaurant).toEqual(expectRestaurantObject)
        })
    })

    describe('func convertRestaurantResponses', () => {
        it('should convert a response array into expect array', () => {
            const restaurants = RestaurantHandler.convertRestaurantResponses(restaurantExamples)

            expect(restaurants).toHaveLength(restaurantExamples.length);
            expect(restaurants[1]).toEqual(expectRestaurantObject)
        })
    })

    describe('func convertCuisineResponse', () => {
        it('should convert the response into the expected object', () => {
            const cuisineType = RestaurantHandler.convertCuisineTypeResponse(restaurantExamples[1].CuisineTypes[0])

            expect(cuisineType).toEqual(expectCuisineType)
        })
    })

    describe('func convertCuisineResponses', () => {
        it('should convert the response array into the expect array of objects', () => {
            const expectedLength = restaurantExamples[1].CuisineTypes.length
            const cuisineTypes = RestaurantHandler.convertCuisineTypeResponses(restaurantExamples[1].CuisineTypes)

            expect(cuisineTypes).toHaveLength(expectedLength)
            expect(cuisineTypes[0]).toEqual(expectCuisineType)
        })
    })
})
