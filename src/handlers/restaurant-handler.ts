import {
    CuisineType,
    CuisineTypeResponse,
    Restaurant,
    RestaurantResponse
} from '../types';

const convertRestaurantResponse = (response: RestaurantResponse): Restaurant => ({
    id: response.Id,
    name: response.Name,
    logo: response.LogoUrl,
    rating: response.RatingStars,
    uniqueName: response.UniqueName,
    cuisineTypes: convertCuisineTypeResponses(response.CuisineTypes)
} as Restaurant)

const convertCuisineTypeResponse = (response: CuisineTypeResponse): CuisineType => ({
    id: response.Id,
    name: response.Name,
    seoName: response.SeoName,
} as CuisineType)

const convertRestaurantResponses = (responses: RestaurantResponse[]): Restaurant[] => (
    responses.map(resp => convertRestaurantResponse(resp))
)

const convertCuisineTypeResponses = (responses: CuisineTypeResponse[]): CuisineType[] => (
    responses.map(resp => convertCuisineTypeResponse(resp))
)

export default {
    convertCuisineTypeResponse,
    convertCuisineTypeResponses,
    convertRestaurantResponse,
    convertRestaurantResponses,
}


