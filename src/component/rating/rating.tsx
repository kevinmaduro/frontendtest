import { Rating } from 'semantic-ui-react';

type RatingProps = {
    rating?: number
}

const RatingComponent = ({ rating = 0 }: RatingProps) => (
    <Rating
        data-testid="rating-semantic"
        icon='star'
        defaultRating={rating}
        maxRating={5}
    />
)

export default RatingComponent;
