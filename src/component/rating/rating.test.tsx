import { render, screen } from '@testing-library/react';
import RatingComponent from './rating';

describe('Rating Component', () => {
    it('should render without errors', () => {
        render(<RatingComponent />);

        expect(screen.getByTestId('rating-semantic')).toBeTruthy()
    })
})
