import { Item } from 'semantic-ui-react';
import { Restaurant } from "../../../types";
import RatingComponent from "../../rating/rating";
import CuisineBadges from "../../cuisine-badges/cuisine-badges";

const RestaurantListItem = ({ id, uniqueName, name, logo, rating, cuisineTypes }: Restaurant) => (
    <Item.Group
        data-testid="card-item"
    >
        <Item>
            <Item.Image size="tiny" src={logo} />

            <Item.Content>
                <Item.Header>{name}</Item.Header>
                <Item.Description>
                    <RatingComponent rating={rating} />
                    <br />
                    <CuisineBadges cuisineTypes={cuisineTypes} />
                </Item.Description>
            </Item.Content>
        </Item>
    </Item.Group>
)

export default RestaurantListItem;
