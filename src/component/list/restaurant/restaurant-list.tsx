import { useEffect } from 'react';
import { List } from "semantic-ui-react";
import { Restaurant } from "../../../types";
import RestaurantListItem from "./restaurant-list-item";

type RestaurantListProps = {
    restaurants: Restaurant[]
}

const RestaurantList = ({ restaurants }: RestaurantListProps) => (
    <List>
        {
            restaurants.map(rest => (
                <List.Item key={rest.id.toString()}>
                    <RestaurantListItem {...rest} />
                </List.Item>
            ))
        }
    </List>
)

export default RestaurantList;
