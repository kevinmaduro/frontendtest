import { List } from 'semantic-ui-react';
import { CuisineType } from "../../types";
import CuisineBadge from "./cuisine-badge";

type CuisineBadgesProps = {
    cuisineTypes: CuisineType[]
}

const generateKeyID = (id: number): number => (
    Math.floor((id + Math.random()) * 100)
)

const CuisineBadges = ({ cuisineTypes }: CuisineBadgesProps) => (
    <List data-testid="cuisine-badge-list" horizontal>
        {
            cuisineTypes.map(cuisineType => (
                <List.Item key={generateKeyID(cuisineType.id)}>
                    <CuisineBadge name={cuisineType.name} />
                </List.Item>
            ))
        }
    </List>
)

export default CuisineBadges;
