import { render, screen } from '@testing-library/react';
import CuisineBadge from "./cuisine-badge";

describe('Cuisine Badge Component' ,() => {
    it('should render without error', () => {
        render(<CuisineBadge name="burguer" />)

        expect(screen.getByTestId('cuisine-badge')).toBeTruthy()
    })

    it('should show cuisine type name', () => {
        render(<CuisineBadge name="burguer" />)

        expect(screen.getByText('burguer')).toBeTruthy()
    })
})
