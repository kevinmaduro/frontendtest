import { render, screen } from '@testing-library/react';
import CuisineBadges from "./cuisine-badges";
import { CuisineType } from "../../types";

const example = [
    {
        name: 'Burgers',
    }
] as CuisineType[]

describe('Cuisine Badges Component', () => {
    it('should render without errors', () => {
        render(<CuisineBadges cuisineTypes={example} />)

        expect(screen.getByTestId('cuisine-badge-list')).toBeTruthy()
    })
})
