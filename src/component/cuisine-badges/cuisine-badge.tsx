import { Label } from "semantic-ui-react";

type CuisineBadgeProps = {
    name: string
}

const CuisineBadge = ({ name }: CuisineBadgeProps) => (
     <Label data-testid="cuisine-badge" color='grey' horizontal>{name}</Label>
)

export default CuisineBadge;
