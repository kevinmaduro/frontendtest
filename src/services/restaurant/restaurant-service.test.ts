import axios from 'axios';
import RestaurantService from './restaurant-service';

jest.mock('axios');

const apiURL = 'http://localhost:8080/restaurants'

const example = {
    "data": {
        "Area": "London",
        "MetaData": {
            "CanonicalName": "ec4m-london",
            "District": "EC4M",
            "Postcode": "EC4M",
            "Area": "London",
        }
    }
}

describe('Restaurant Service', () => {
    describe('func getByPostCode', () => {
        it('should return data when request successfully', async () => {
            // @ts-ignore
            axios.get.mockResolvedValueOnce(example)

            const result = await RestaurantService.getByPostCode()

            expect(axios.get).toHaveBeenCalledWith(apiURL);
            expect(result).toEqual(example.data);
        })

        it('should return nothing when request fails', async () => {
            const message = "Error";
            // @ts-ignore
            axios.get.mockRejectedValueOnce(message)

            const result = await RestaurantService.getByPostCode();

            expect(axios.get).toHaveBeenCalledWith(apiURL)
            expect(result).toEqual(message)
        })

    })
})
