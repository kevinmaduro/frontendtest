import axios from 'axios';
import { ApiResponse } from "../../types";

const getByPostCode = async () => (
    await axios.get<ApiResponse>('http://localhost:8080/restaurants')
        .then(resp => resp.data)
        .catch(err => err)
)

export default {
    getByPostCode,
}
