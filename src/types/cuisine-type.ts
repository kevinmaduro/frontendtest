export default interface CuisineType {
    id: number,
    name: string,
    seoName: string,
}
