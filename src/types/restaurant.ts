import CuisineType from './cuisine-type'

export default interface Restaurant {
    id: number,
    name: string,
    uniqueName: string,
    rating: number,
    logo: string,
    cuisineTypes: CuisineType[]
}
