import CuisineTypeResponse from './cuisine-type-response';

export default interface RestaurantResponse {
    Id: number,
    Name: string,
    UniqueName: string,
    RatingStars: number,
    LogoUrl: string,
    CuisineTypes: CuisineTypeResponse[],
}
