export default interface CuisineTypeResponse {
    Id: number,
    Name: string,
    SeoName: string,
}
