## Observation
I would like to say that the Open API has region block in Brazil, so I couldn't be able
to use the API freely. Because of that, I get the json using the ec4m to use it as
example. I added json-server to serve that file to simulate an API. I know
that it wouldn't be the same idea as the Open API, but I used what I could without using
a VPN (since it could take me even more hours than I expect).

1. How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.
> 7 Hours. I used toggl app to track my time.
> If I had more that I would fix the search to simulate a request to the API.
> I would increase the test coverage using more event tests in the searchPage (I'm used to Jest/Enzyme)
> I would create a hook to deal with restaurants data.
> I would improve the page styling to enhance user experience, using mobile first for the pages
> I would improve the error catching with the API returns and do unit tests for it, to prevent breaking the app
> I would add Cypress to do an interface testing
> I would separate in more small components some big snippets.
> I would add pagination to the website to handle better with a big group of data.
> Since the API doens't work here, I would say that I could add a route for search ByPostCode
> I would add an input to get the postCode typed by the user and show the corresponding restaurants in that location

2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
> The Restaurant Handler, I usually create this kind of handler to deal with data when
> APIs can't change their contract response. So I use those handlers to convert the objects
> in a better and readable code object.
```typescript
const convertRestaurantResponse = (response: RestaurantResponse): Restaurant => ({
    id: response.Id,
    name: response.Name,
    logo: response.LogoUrl,
    rating: response.RatingStars,
    uniqueName: response.UniqueName,
    cuisineTypes: convertCuisineTypeResponses(response.CuisineTypes)
} as Restaurant)
```
The usage:
```typescript
getRestaurants().then(respRestaurants => {
                const convertedRestaurants = RestaurantHandler.convertRestaurantResponses(
                    respRestaurants.Restaurants
                )
                setRestaurants(convertedRestaurants)
            })
```

I know that I could do it in the service, but I want respect the single responsibilities'
principle. So I thought doing this in the service would break this principle.

3. How would you track down a performance issue in production? Have you ever had to do this?
> I would track using NewRelic or GA. Yes, I already did this. In Smart TV platform I needed to
> track down those issues, but we couldn't use NewRelic because of some browser versions
> in the platform.
